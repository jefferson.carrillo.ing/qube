import { Component, OnInit } from '@angular/core';
import { PeticionModel } from 'src/app/models/PeticionModel';
import { SystemService } from '../app/services/System.service';
import { LanguageService } from '../app/services/Language.service';
import { Utilitarian } from 'src/app/services/Utilitarian';

declare var $: any;
declare var document: any;
declare var Pace: any;
declare var location: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'QUBE';
  constructor(private systemService: SystemService, private languageService: LanguageService) { }
  ngOnInit(): void {
    this.ShowListLanguage();

    $(document).ajaxStart(function () {
      Pace.restart();
    });


  }


  ShowListSystem(): void {
    this.systemService.showList().subscribe(
      (dato: PeticionModel) => {
        var ListSystem = dato.data.objSystem;
        Utilitarian.SetSystemData(ListSystem);
        this.checkLocalData();
      },
      (error: any) => {
        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });
      }
    );
  }


  ShowListLanguage(): void {
    this.languageService.showList().subscribe(
      (dato: PeticionModel) => {
        var ListLanguage = dato.data.objLanguage;
        Utilitarian.SetLanguageData(ListLanguage);
        this.ShowListSystem();
      },
      (error: any) => {
        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });
      }
    );
  }

  checkLocalData() {
    setTimeout(() => { 
      let name = $("#id_title_sistem")[0].innerText;
      name = name.replace(/\s+/g, '');
      let name_default = Utilitarian.strValueDefaut;
      name_default = name_default.replace(/\s+/g, '');
      if (name == name_default) {
        location.reload(true);
      }

    }, 5);
  }


}
