import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParameterModel } from '../models/ParameterModel';
import { PeticionModel } from '../models/PeticionModel';
import { ContactModel } from '../models/ContactModel';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(private _HttpClient: HttpClient) { }

  private link_save = ParameterModel.url_root + '/contact/save';

  public save(contact: ContactModel): Observable<PeticionModel> {
    return this._HttpClient.post<PeticionModel>(this.link_save, contact);
  }

}
