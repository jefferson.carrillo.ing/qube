import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParameterModel } from '../models/ParameterModel';
import { PeticionModel } from '../models/PeticionModel';
import { SystemModel } from '../models/SystemModel';
import { Utilitarian } from './Utilitarian';

@Injectable({
  providedIn: 'root',
})
export class SystemService {
  constructor(private _HttpClient: HttpClient) { }

  private link_save = ParameterModel.url_root + '/system/save';
  private link_show = ParameterModel.url_root + '/system/show';
  private link_update = ParameterModel.url_root + '/system/update';

  public save(System: SystemModel): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    System.id_language = id_language;
    return this._HttpClient.post<PeticionModel>(this.link_save, System);
  }

  public update(System: SystemModel): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    System.id_language = id_language;
    return this._HttpClient.post<PeticionModel>(this.link_update, System);
  }

  public show(key: any): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    var parameter = { id_language, filter: { condition: 1, key } };
    return this._HttpClient.post<PeticionModel>(this.link_show, parameter);
  }

  public showList(): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    var parameter = { id_language, filter: { condition: 1 } };
    return this._HttpClient.post<PeticionModel>(this.link_show, parameter);
  }

}
