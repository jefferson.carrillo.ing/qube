import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 
import { ParameterModel } from '../models/ParameterModel';
import { PeticionModel } from '../models/PeticionModel';
import { QuestionModel } from '../models/QuestionModel';

@Injectable({
  providedIn: 'root',
})
export class QuestionService { 
  constructor(private _HttpClient: HttpClient) {}
 
  private link_save =  ParameterModel.url_root + '/question/save'; 
 
  public save(question: QuestionModel): Observable<PeticionModel> {
    return this._HttpClient.post<PeticionModel>(this.link_save, question);
  }
 
}
