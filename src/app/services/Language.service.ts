import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParameterModel } from '../models/ParameterModel';
import { PeticionModel } from '../models/PeticionModel';
import { LanguageModel } from '../models/LanguageModel';
import { Utilitarian } from './Utilitarian';

@Injectable({
  providedIn: 'root',
})
export class LanguageService { 
  constructor(private _HttpClient: HttpClient) { }

  private link_save =  ParameterModel.url_root + '/language/save';
  private link_show =  ParameterModel.url_root + '/language/show';

  public save(Language: LanguageModel): Observable<PeticionModel> {
    return this._HttpClient.post<PeticionModel>(this.link_save, Language);
  }

  public showList(): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    var parameter = { id_language, filter: { id_language } };
    return this._HttpClient.post<PeticionModel>(this.link_show, parameter);
  }

}
