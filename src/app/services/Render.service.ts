import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParameterModel } from '../models/ParameterModel';
import { PeticionModel } from '../models/PeticionModel';
import { RenderModel } from '../models/RenderModel';
import { Utilitarian } from './Utilitarian';

@Injectable({
  providedIn: 'root',
})
export class RenderService { 
  constructor(private _HttpClient: HttpClient) { }

  private link_save = ParameterModel.url_root + '/render/save';
  private link_show = ParameterModel.url_root + '/render/show';
  private link_update = ParameterModel.url_root + '/render/update';

  public save(Render: RenderModel): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    Render.id_language = id_language;
    return this._HttpClient.post<PeticionModel>(this.link_save, Render);
  }

  public update(Render: RenderModel): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    Render.id_language = id_language;
    return this._HttpClient.post<PeticionModel>(this.link_update, Render);
  }

  public show(key: any): Observable<PeticionModel> {
    var objIdioma = Utilitarian.GetIdLanguages();
    let id_language = objIdioma.id;
    var parameter = { id_language, filter: { id_language, key } };
    return this._HttpClient.post<PeticionModel>(this.link_show, parameter);
  }

}
