export class RenderModel {

    public id: Number;
    public key: String;
    public title: String;
    public icon: String;
    public description: String;
    public id_language: Number;
    public condition: String;
    public type: String;
    public content: String;




    constructor() {
        this.id = 0;
        this.key = "";
        this.title = "";
        this.icon = "";
        this.description = "";
        this.id_language = 0;
        this.condition = "";
        this.type = "";
        this.content = "";
    }





}
