import { Utilitarian } from 'src/app/services/Utilitarian';
import { ParameterModel } from '../models/ParameterModel';

export class GeneralModel {
  public url_root = ParameterModel.url_root;
  public name: String;
  public version: String;
  public name_full: string;
  public phone1: string;
  public phone2: string;
  public email: string;
  public reference: string;
  public direction: string;
  public decription: string;
  public img_home: string;
  public anio: string;

  public text_video: string;
  public text_intro: string;
  public text_full: string;

  public url_intro: string;
  public url_full: string;

  public url_facebook: string;
  public url_instagram: string;
  public url_linkedin: string;
  public url_twitter: string;

  public text_version: string;
  public text_reserved: string;


  public text_login: string;
  public text_register: string;
  public text_home: string;
  public text_sales: string;
  public text_about: string;
  public text_model: string;

  public text_active: string;
  public text_active_listings: string;

  public text_contact_name: string;
  public text_contact_description: string;
  public url_contact_img: string;
  public url_plan: string;
  constructor() {
    this.name = Utilitarian.GetSystemData("name");
    this.version = Utilitarian.GetSystemData("version");
    this.name_full = Utilitarian.GetSystemData("name_full");
    this.phone1 = Utilitarian.GetSystemData("phone1");
    this.phone2 = Utilitarian.GetSystemData("phone2");
    this.email = Utilitarian.GetSystemData("email");
    this.reference = Utilitarian.GetSystemData("reference");
    this.direction = Utilitarian.GetSystemData("direction");
    this.decription = Utilitarian.GetSystemData("decription");
    this.img_home = Utilitarian.GetSystemData("img_home");
    this.anio = Utilitarian.GetSystemData("anio");
    this.text_video = Utilitarian.GetSystemData("text_video");
    this.url_intro = this.url_root + Utilitarian.GetSystemData("url_intro");
    this.url_full = this.url_root + Utilitarian.GetSystemData("url_full");
    this.text_intro = Utilitarian.GetSystemData("text_intro");
    this.text_full = Utilitarian.GetSystemData("text_full");
    this.text_version = Utilitarian.GetSystemData("text_version");
    this.text_reserved = Utilitarian.GetSystemData("text_reserved");


    this.url_facebook = Utilitarian.GetSystemData("url_facebook");
    this.url_instagram = Utilitarian.GetSystemData("url_instagram");
    this.url_linkedin = Utilitarian.GetSystemData("url_linkedin");
    this.url_twitter = Utilitarian.GetSystemData("url_twitter");


    this.text_login = Utilitarian.GetSystemData("text_login");
    this.text_register = Utilitarian.GetSystemData("text_register");
    this.text_home = Utilitarian.GetSystemData("text_home");
    this.text_sales = Utilitarian.GetSystemData("text_sales");
    this.text_about = Utilitarian.GetSystemData("text_about");
    this.text_model = Utilitarian.GetSystemData("text_model");
    this.text_active = Utilitarian.GetSystemData("text_active");
    this.text_active_listings=Utilitarian.GetSystemData("text_active_listings");


    this.text_contact_name = Utilitarian.GetSystemData("text_contact_name");
    this.text_contact_description = Utilitarian.GetSystemData("text_contact_description");
    this.url_contact_img = this.url_root + Utilitarian.GetSystemData("url_contact_img");

    this.url_plan = this.url_root + Utilitarian.GetSystemData("url_plan");

  }

}
