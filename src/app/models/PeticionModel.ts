export class PeticionModel {
  status: number;
  message: String;
  data: any;

  constructor(status: number, message: String, data: any) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}
