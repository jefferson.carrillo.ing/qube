import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { AdministradorComponent } from './component/administrador/administrador.component';
import { AboutComponent } from './component/about/about.component';
import { FooterComponent } from './component/main/footer/footer.component';
import { HeaderComponent } from './component/main/header/header.component';
import { SuiteComponent } from './component/suite/suite.component';

import { FormsModule } from '@angular/forms';
import { BoxActiveListingsComponent } from './component/specific/box-active-listings/box-active-listings.component';
import { BoxContactUsComponent } from './component/specific/box-contact-us/box-contact-us.component';
import { BoxAskedQuestionsComponent } from './component/specific/box-asked-questions/box-asked-questions.component';
import { BoxGlobalInfoUnoComponent } from './component/specific/box-global-info-uno/box-global-info-uno.component';
import { BoxGlobalLocationComponent } from './component/specific/box-global-location/box-global-location.component';
import { BoxGlobalGeolocationComponent } from './component/specific/box-global-geolocation/box-global-geolocation.component';
import { BoxSimilaryComponent } from './component/specific/box-similary/box-similary.component';
import { BoxCarruselComponent } from './component/specific/box-carrusel/box-carrusel.component';
import { BoxGlobalContentListComponent } from './component/specific/box-global-content-list/box-global-content-list.component';
import { BoxGlobalContentTextComponent } from './component/specific/box-global-content-text/box-global-content-text.component';
import { BoxGlobalContentBlockComponent } from './component/specific/box-global-content-block/box-global-content-block.component';
import { BoxGlobalInfoDosComponent } from './component/specific/box-global-info-dos/box-global-info-dos.component';
import { BoxGlobalInfoTresComponent } from './component/specific/box-global-info-tres/box-global-info-tres.component';
import { BoxGlobalLineComponent } from './component/specific/box-global-line/box-global-line.component';
import { BoxGlobalInfoTeamComponent } from './component/specific/box-global-info-team/box-global-info-team.component';
import { BoxRenderComponent } from './component/specific/box-render/box-render.component';

import { BoxFloorPlanComponent } from './component/specific/box-floor-plan/box-floor-plan.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SalesComponent } from './component/sales/sales.component';
import {HttpClientModule} from '@angular/common/http';

import { CountToModule } from 'angular-count-to'; 
import { ModelComponent } from './component/model/model.component';
import { BoxSectionComponent } from './component/specific/box-section/box-section.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AdministradorComponent,
    AboutComponent,
    FooterComponent,
    HeaderComponent,
    SuiteComponent,
    BoxActiveListingsComponent,
    BoxContactUsComponent,
    BoxAskedQuestionsComponent,
    BoxGlobalInfoUnoComponent,
    BoxGlobalLocationComponent,
    BoxGlobalGeolocationComponent,
    BoxSimilaryComponent,
    BoxCarruselComponent,
    BoxGlobalContentListComponent,
    BoxGlobalContentTextComponent,
    BoxGlobalContentBlockComponent,
    BoxGlobalInfoDosComponent,
    BoxGlobalInfoTresComponent,
    BoxGlobalLineComponent,
    BoxGlobalInfoTeamComponent,
    BoxFloorPlanComponent,
    SalesComponent,
    BoxRenderComponent, 
    ModelComponent, BoxSectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    CountToModule, 
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {

   
}
