import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';

import { HomeComponent } from './component/home/home.component';
import { SalesComponent } from './component/sales/sales.component';
import { AboutComponent } from './component/about/about.component';
import { SuiteComponent } from './component/suite/suite.component';
import { AdministradorComponent } from './component/administrador/administrador.component';
import { ModelComponent } from './component/model/model.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'sales', component: SalesComponent },
  { path: 'about', component: AboutComponent },
  { path: 'model', component: ModelComponent},
  { path: 'suite', component: SuiteComponent },
  { path: 'admin', component: AdministradorComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
