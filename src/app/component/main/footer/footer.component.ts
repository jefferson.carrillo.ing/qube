import { Component,  OnInit, Output, EventEmitter, Input } from '@angular/core';
import { GeneralModel } from '../../../models/GeneralModel';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
  @Input()  public dataGeneral:  GeneralModel | any ;
  constructor() {}

  ngOnInit(): void {}
}
