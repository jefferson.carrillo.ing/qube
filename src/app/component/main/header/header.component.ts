import { Component, OnInit, Input } from '@angular/core';
import { GeneralModel } from '../../../models/GeneralModel';
import { Utilitarian } from './../../../services/Utilitarian';
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Input() public dataGeneral: GeneralModel | any;
  public dataMenus: any = [];
  constructor() { }

  ngOnInit(): void {
    this.CambiarIdioma();

    this.dataMenus = [
      {
        name: this.dataGeneral.text_home,
        link: '/home',
      },
      {
        name: this.dataGeneral.text_sales,
        link: '/sales',
      },
      {
        name: this.dataGeneral.text_about,
        link: '/about',
      },
      {
        name: this.dataGeneral.text_model,
        link: '/model',
      },
    ];
  }


  public selectedLenguageValue = Utilitarian.GetIdLanguages();
  public Languages: any = Utilitarian.GetLanguageData();

  CambiarIdioma() {
    var Languages: any = this.Languages;
    var url_root: any = this.dataGeneral.url_root;
    var estilo = function (idioma: any) {

      let objIdioma: any = Languages.filter(function (n: any) {
        return n.value === idioma.id;
      });
      if (objIdioma.length != 0) {
        objIdioma = objIdioma[0];
        var $span = $(
          '<span><img style="width: 30px;height: 17px;margin-top: -5px;" src="' + url_root + objIdioma.img + '"/> ' + idioma.text + '</span>'
        );
        return $span;
      }
    };


    $('#id-select-idiomas')
      .select2({
        templateResult: estilo,
        templateSelection: estilo,
      })
      .on('select2:select', function (e: any) {
        debugger
        let idioma = $('#id-select-idiomas').val();
        let objIdioma: any = Languages.filter(function (n: any) {
          return n.value === idioma;
        });
        objIdioma = objIdioma[0];
        let textIdioma = JSON.stringify(objIdioma);
        localStorage.setItem('id_languages-active', textIdioma);
        setTimeout(() => {
          location.reload();
        }, 200);
      });


    $('#id-select-idiomas-min')
      .select2({
        'width': '35%',
        'margin-top': '10px',
        templateResult: estilo,
        templateSelection: estilo,
      })
      .on('select2:select', function (e: any) {
        debugger
        let idioma = $('#id-select-idiomas-min').val();
        let objIdioma: any = Languages.filter(function (n: any) {
          return n.value === idioma;
        });
        objIdioma = objIdioma[0];
        let textIdioma = JSON.stringify(objIdioma);
        localStorage.setItem('id_languages-active', textIdioma);
        setTimeout(() => {
          location.reload();
        }, 200);
      });




    var objIdioma = Utilitarian.GetIdLanguages();
    this.selectedLenguageValue = objIdioma.value;


  }



}
