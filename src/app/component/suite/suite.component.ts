import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralModel } from '../../models/GeneralModel';
import { Building, Availabilitys } from './../../../../data_base';

@Component({
  selector: 'app-suite',
  templateUrl: './suite.component.html',
  styleUrls: ['./suite.component.css'],
})
export class SuiteComponent implements OnInit {

  public dataGeneral: GeneralModel = new GeneralModel();
  public availabilitys = Availabilitys;
  public building = Building;
  public suite: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }


  public dataBoxRender: any = [
    //carusel
    { section: "left", box_class: "in", type: "building_facts", key: "#building_facts" },
    { section: "left", box_class: "in", type: "amenities", key: "#amenities" },
    { section: "left", box_class: "in", type: "nearby", key: "#nearby" },

    { section: "right", box_class: "in", type: "similary", key: "#similary" },
    //pp-box-global-info-dos
    { section: "right", box_class: "in", type: "html", key: "#sale-office" },
    { section: "right", box_class: "in", type: "contact", key: "#contact-us" },

    { section: "bottom", box_class: "in", type: "active-listings", key: "#active-listings" },
    { section: "bottom", box_class: "in", type: "asked-questions", key: "#asked-questions" },
  ]








  public data_carrusel_suite = {
    breadcrumb: this.building.categorys,
    images: [],
  };

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams.id;
    this.suite = Building.suites[id - 1];

    var list_image = this.suite.images;
    list_image.forEach((e: any) => {
      e.url = `assets/img_web/Suite${this.suite.room}/${e.url}`;
    });
    this.data_carrusel_suite.images = list_image;
    this.data_carrusel_suite.breadcrumb.push(this.building.name);
    this.data_carrusel_suite.breadcrumb.push(`${this.suite.type} (${this.suite.floor}${this.suite.room}) ${this.suite.unit}`);
  }
}
