import { Component, OnInit } from '@angular/core';
import { GeneralModel } from '../../models/GeneralModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public dataGeneral: GeneralModel = new GeneralModel();
  constructor() { }
  ngOnInit(): void {
  }


  public dataBoxRender = [
    { section: "left", box_class: "in", type: "html", key: "#topical-loving" },
    { section: "left", box_class: "in", type: "html", key: "#sale-office" },
    { section: "left", box_class: "in", type: "info", key: "#active_listings" },

    { section: "right", box_class: "in", type: "contact", key: "#contact-us" },
    { section: "right", box_class: "in", type: "amenities", key: "#amenities" },
    { section: "right", box_class: " ", type: "geolocation", key: "#geolocation" },

    { section: "bottom", box_class: "in", type: "plan", key: "#floor-plan" },
    { section: "bottom", box_class: "in", type: "active-listings", key: "#active-listings" },
    { section: "bottom", box_class: "in", type: "similary", key: "#similary" },
    { section: "bottom", box_class: "in", type: "asked-questions", key: "#asked-questions" },

  ]




}
