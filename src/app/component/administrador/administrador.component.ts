import { Component, OnInit } from '@angular/core';
import { GeneralModel } from '../../models/GeneralModel';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {
  public dataGeneral: GeneralModel = new GeneralModel();
 

  constructor() { }

  ngOnInit(): void {
  }

}
