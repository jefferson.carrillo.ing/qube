import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralModel } from 'src/app/models/GeneralModel';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public dataGeneral: GeneralModel = new GeneralModel();
  constructor( private router: Router ) { }

  ngOnInit(): void {
  }

  public Login() {
    this.router.navigate(['/home', {  }]);

  }

}
