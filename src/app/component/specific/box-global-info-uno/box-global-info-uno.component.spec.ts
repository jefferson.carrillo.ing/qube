import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalInfoUnoComponent } from './box-global-info-uno.component';

describe('BoxGlobalInfoUnoComponent', () => {
  let component: BoxGlobalInfoUnoComponent;
  let fixture: ComponentFixture<BoxGlobalInfoUnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalInfoUnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalInfoUnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
