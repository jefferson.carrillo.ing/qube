import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-info-uno',
  templateUrl: './box-global-info-uno.component.html',
  styleUrls: ['./box-global-info-uno.component.css'] 
})

 
export class BoxGlobalInfoUnoComponent implements OnInit {
  @Input() public data: any; 
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() { }

  ngOnInit(): void {
  }

  public count = { 
    from: 0,
    duration: 3
};



  public JumpTo = Utilitarian.JumpTo;
}
