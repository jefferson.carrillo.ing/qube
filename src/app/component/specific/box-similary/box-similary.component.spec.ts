import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxSimilaryComponent } from './box-similary.component';

describe('BoxSimilaryComponent', () => {
  let component: BoxSimilaryComponent;
  let fixture: ComponentFixture<BoxSimilaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxSimilaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxSimilaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
