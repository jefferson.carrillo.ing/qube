import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';
declare var $: any;

@Component({
  selector: 'app-box-similary',
  templateUrl: './box-similary.component.html',
  styleUrls: ['./box-similary.component.css'],
})
export class BoxSimilaryComponent implements OnInit {
  @Input() public availabilitys: any;
  @Input() public building: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      //https://www.jqueryscript.net/demo/Fully-Responsive-Flexible-jQuery-Carousel-Plugin-slick/
      $('#id_slider' + this.id_box).slick({
        dots: false,
        infinite: true,
        centerMode: true,
        variableWidth: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 6000,
      });
    }, 100);
  }
  public JumpTo = Utilitarian.JumpTo;
}
