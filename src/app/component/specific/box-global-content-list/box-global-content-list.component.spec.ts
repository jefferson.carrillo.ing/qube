import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalContentListComponent } from './box-global-content-list.component';

describe('BoxGlobalContentListComponent', () => {
  let component: BoxGlobalContentListComponent;
  let fixture: ComponentFixture<BoxGlobalContentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalContentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalContentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
