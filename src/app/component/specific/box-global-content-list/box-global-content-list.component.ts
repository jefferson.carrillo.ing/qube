import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-content-list',
  templateUrl: './box-global-content-list.component.html',
  styleUrls: ['./box-global-content-list.component.css'],
})
export class BoxGlobalContentListComponent implements OnInit {
  @Input() public data: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
