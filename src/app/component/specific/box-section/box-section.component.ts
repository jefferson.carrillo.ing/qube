import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-box-section',
  templateUrl: './box-section.component.html',
  styleUrls: ['./box-section.component.css']
})
export class BoxSectionComponent implements OnInit {
  @Input() public dataBoxRender: any;
  public dataBoxRenderTop: any = [];
  public dataBoxRenderLeft: any = [];
  public dataBoxRenderRight: any = [];
  public dataBoxRenderBottom: any = [];
  constructor() { }

  ngOnInit(): void {


    this.dataBoxRenderTop = this.dataBoxRender.filter((e: any) => { return e.section == "top"; });
    this.dataBoxRenderLeft = this.dataBoxRender.filter((e: any) => { return e.section == "left"; });
    this.dataBoxRenderRight = this.dataBoxRender.filter((e: any) => { return e.section == "right"; });
    this.dataBoxRenderBottom = this.dataBoxRender.filter((e: any) => { return e.section == "bottom"; });
  }

}
