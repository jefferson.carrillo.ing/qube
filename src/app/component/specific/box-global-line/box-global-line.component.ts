import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-line',
  templateUrl: './box-global-line.component.html',
  styleUrls: ['./box-global-line.component.css']
})
export class BoxGlobalLineComponent implements OnInit {
  @Input() public data: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() { }

  ngOnInit(): void {
  }

}
