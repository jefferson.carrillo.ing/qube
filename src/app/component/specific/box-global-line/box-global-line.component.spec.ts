import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalLineComponent } from './box-global-line.component';

describe('BoxGlobalLineComponent', () => {
  let component: BoxGlobalLineComponent;
  let fixture: ComponentFixture<BoxGlobalLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalLineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
