import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxAskedQuestionsComponent } from './box-asked-questions.component';

describe('BoxAskedQuestionsComponent', () => {
  let component: BoxAskedQuestionsComponent;
  let fixture: ComponentFixture<BoxAskedQuestionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxAskedQuestionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxAskedQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
