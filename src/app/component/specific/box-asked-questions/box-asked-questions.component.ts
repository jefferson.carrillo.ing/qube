import { Component, Input, OnInit } from '@angular/core';
import { PeticionModel } from 'src/app/models/PeticionModel';
import { Utilitarian } from 'src/app/services/Utilitarian';
import { QuestionModel } from '../../../models/QuestionModel';
import { QuestionService } from '../../../services/Question.service';

@Component({
  selector: 'app-box-asked-questions',
  templateUrl: './box-asked-questions.component.html',
  styleUrls: ['./box-asked-questions.component.css'],
})
export class BoxAskedQuestionsComponent implements OnInit {
  @Input() public availabilitys: any;
  @Input() public building: any;
  public id_box = Utilitarian.GenerateKeyHex();

  public questionModel:QuestionModel = new QuestionModel();

  ngOnInit(): void {}
  constructor(private questionService:QuestionService) {}




  FormQuestion() {
    debugger;
    var description: any = this.questionModel.description;
    var message_send: any = JSON.parse(
      localStorage.getItem('message_send_question') || '[]'
    );
    let hast = Utilitarian.ConvertHex(description);
    var existe = message_send.filter((e: any) => {
      return e == hast;
    });
    if (existe.length == 0) {
       this.SaveQuestion(message_send, hast);
    } else {
      Utilitarian.Alertify_Alert({
        text: 'This question has already been sent.',
        type: 'warning',
      });
    }
  }

  
  SaveQuestion(message_send: any, hast: any): void { 
    this.questionService.save(this.questionModel).subscribe(
      (dato: PeticionModel) => {       
        this.questionModel = dato.data;
        this.ClearForm();
        message_send.push(hast);
        localStorage.setItem('message_send_question', JSON.stringify(message_send));
        Utilitarian.Alertify_Alert({
          text: dato.message,
          type: 'success',
        });
  
      },
      (error: any) => {
        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });

      }
    );
  }

  ClearForm() {
    this.questionModel = new QuestionModel();
  }
}
