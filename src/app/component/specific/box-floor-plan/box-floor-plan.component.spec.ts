import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxFloorPlanComponent } from './box-floor-plan.component';

describe('BoxFloorPlanComponent', () => {
  let component: BoxFloorPlanComponent;
  let fixture: ComponentFixture<BoxFloorPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxFloorPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxFloorPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
