import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-floor-plan',
  templateUrl: './box-floor-plan.component.html',
  styleUrls: ['./box-floor-plan.component.css'],
})
export class BoxFloorPlanComponent implements OnInit { 
 
  @Input() public dataGeneral: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
