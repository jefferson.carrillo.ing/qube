import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxContactUsComponent } from './box-contact-us.component';

describe('BoxContactUsComponent', () => {
  let component: BoxContactUsComponent;
  let fixture: ComponentFixture<BoxContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
