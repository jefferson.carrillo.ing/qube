import { Component, Input, OnInit } from '@angular/core';
import { PeticionModel } from 'src/app/models/PeticionModel';
import { Utilitarian } from 'src/app/services/Utilitarian';
import { ContactModel } from '../../../models/ContactModel';
import { ContactService } from '../../../services/Contact.service';

@Component({
  selector: 'app-box-contact-us',
  templateUrl: './box-contact-us.component.html',
  styleUrls: ['./box-contact-us.component.css'],
})
export class BoxContactUsComponent implements OnInit {
 
  @Input() public data: any;
  @Input() public dataGeneral: any;
  public id_box = Utilitarian.GenerateKeyHex();

  public contactModel: ContactModel = new ContactModel();

  ngOnInit(): void { }
  constructor(private contactService: ContactService) { }



  FormContact() {
    var email: any = this.contactModel.email;
    var description: any = this.contactModel.description;
    var message_send: any = JSON.parse(
      localStorage.getItem('message_send') || '[]'
    );
    let hast: any = Utilitarian.ConvertHex(email + description);
    var existe = message_send.filter((e: any) => {
      return e == hast;
    });
    if (existe.length == 0) {
      this.SaveContact(message_send, hast);
    } else {
      Utilitarian.Alertify_Alert({
        text: 'This message has already been sent.',
        type: 'warning',
      });
    }
  }

  SaveContact(message_send: any, hast: any): void {
    this.contactService.save(this.contactModel).subscribe(
      (dato: PeticionModel) => {
        debugger;
        this.contactModel = dato.data;
        this.ClearForm();
        message_send.push(hast);
        localStorage.setItem('message_send', JSON.stringify(message_send));
        Utilitarian.Alertify_Alert({
          text: dato.message,
          type: 'success',
        });

      },
      (error: any) => {
        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });

      }
    );
  }

  ClearForm() {
    this.contactModel = new ContactModel();
  }

}
