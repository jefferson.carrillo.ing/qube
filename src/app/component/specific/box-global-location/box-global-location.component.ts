import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-location',
  templateUrl: './box-global-location.component.html',
  styleUrls: ['./box-global-location.component.css']
})
export class BoxGlobalLocationComponent implements OnInit {
  @Input() public availabilitys: any;
  @Input() public building: any; 
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() { }

  ngOnInit(): void {
  }

}
