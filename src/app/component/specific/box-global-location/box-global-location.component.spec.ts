import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalLocationComponent } from './box-global-location.component';

describe('BoxGlobalLocationComponent', () => {
  let component: BoxGlobalLocationComponent;
  let fixture: ComponentFixture<BoxGlobalLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalLocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
