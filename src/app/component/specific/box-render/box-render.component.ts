import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';
import { PeticionModel } from 'src/app/models/PeticionModel';
import { RenderService } from '../../../services/Render.service';
import { RenderModel } from '../../../models/RenderModel';
import { GeneralModel } from '../../../models/GeneralModel';
import { Building, Availabilitys } from './../../../../../data_base';
@Component({
  selector: 'app-box-render',
  templateUrl: './box-render.component.html',
  styleUrls: ['./box-render.component.css']
})
export class BoxRenderComponent implements OnInit {

  @Input() public data: any;

  public key: any;
  public type: any;
  public box_class: any;



  public id_box = Utilitarian.GenerateKeyHex();
  public renderModel: RenderModel = new RenderModel();
  public static i: any;
  public dataGeneral: GeneralModel = new GeneralModel();
  constructor(private renderService: RenderService) {

  }

  public availabilitys = Availabilitys;
  public building = Building;

  ngOnInit(): void {
    this.key = this.data.key;
    this.type = this.data.type;
    this.box_class = this.data.box_class;
    BoxRenderComponent.i = this;
    this.ShowRender(this.key);

  }




  ShowRender(key: any): void {
    this.renderModel.id = 0;
    this.renderModel.key = key;
    this.renderModel.icon = "fa fa-ban";
    this.renderModel.title = 'No se encontro data en <code>' + key + '</code>';

    this.renderService.show(key).subscribe(
      (dato: PeticionModel) => {
        this.renderModel = dato.data.objRender;
      },
      (error: any) => {
        this.renderModel.icon = "fa fa-bug";
        this.renderModel.title = 'Ha ocurrido un error en <code>' + key + '</code>';
        this.renderModel.content = '<p class="margin">' + error.message + '</p>';

        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });
      }
    );
  }

  public async OpenEdit() {
    var callBack = null;
    BoxRenderComponent.i = this;

    if (BoxRenderComponent.i.renderModel.id == 0) {
      callBack = BoxRenderComponent.i.SaveRender;
    } else {
      callBack = BoxRenderComponent.i.UpdateRender;
    }
    if (this.type == "html") {
      await Utilitarian.Alertify_Modal_Editor(BoxRenderComponent.i.renderModel, callBack);
    } else {
      await Utilitarian.Alertify_Modal_Datos(BoxRenderComponent.i.renderModel, callBack);
    }


  }

  public UpdateRender(renderModel: RenderModel): void {
    BoxRenderComponent.i.renderService.update(renderModel).subscribe(
      (dato: PeticionModel) => {
        BoxRenderComponent.i.renderModel = dato.data.objRender;
        Utilitarian.Alertify_Alert({
          text: dato.message,
          type: 'success',
        });
      },
      (error: any) => {
        BoxRenderComponent.i.renderModel.icon = "fa fa-bug";
        BoxRenderComponent.i.renderModel.title = 'Ha ocurrido un error en <code>' + renderModel.key + '</code>';
        BoxRenderComponent.i.renderModel.content = '<p class="margin">' + error.message + '</p>';

        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });
      }
    );
  }

  public SaveRender(renderModel: RenderModel): void {
    BoxRenderComponent.i.renderService.save(renderModel).subscribe(
      (dato: PeticionModel) => {
        BoxRenderComponent.i.renderModel = dato.data.objRender;
        Utilitarian.Alertify_Alert({
          text: dato.message,
          type: 'success',
        });
      },
      (error: any) => {
        BoxRenderComponent.i.renderModel.icon = "fa fa-bug";
        BoxRenderComponent.i.renderModel.title = 'Ha ocurrido un error en <code>' + renderModel.key + '</code>';
        BoxRenderComponent.i.renderModel.content = '<p class="margin">' + error.message + '</p>';

        Utilitarian.Alertify_Alert({
          text: error.message,
          type: 'error',
        });
      }
    );
  }


  //RESTRUCTURAR DESDE AQUI
  public data_info_uno = {
    text_active: this.dataGeneral.text_active,
    text_active_listings: this.dataGeneral.text_active_listings,
    not_available: this.building.suites.filter((e: any) => {
      return e.id_availability == 0;
    }).length,
    available: this.building.suites.filter((e: any) => {
      return e.id_availability == 1;
    }).length,
    in_contract: this.building.suites.filter((e: any) => {
      return e.id_availability == 2;
    }).length,
  };



  public data_we_are_future = {
    title: 'We are the Future',
    icon: 'fa fa-rocket',
    content: this.building.about.we_are_the_future,
  };

  public data_line = {
    content: this.building.about.our_teams,
  };

  public data_team = {
    content: this.building.about.teams,
  };

  public data_what_we_do = {
    title: 'What we do',
    icon: 'fa fa-suitcase',
    content: this.building.about.what_we_do,
  };

  public data_awards = {
    title: 'Awards',
    icon: 'fa fa-trophy',
    subtitle:
      'We will receive awards for:',
    list: this.building.about.awards,
  };

  public data_company_information = {
    title: 'Company Information',
    icon: 'fa fa-black-tie',
    content: this.building.company_information,
  };





  public data_description = {
    title: 'Tropical Living',
    content: this.building.description,
    icon: 'fa fa-globe',
  };

  public data_sales_office = {
    title: 'Rent and Sales Office',
    blocks: this.building.sales_offices,
    icon: 'fa fa-map-pin',
  };

  public data_location = {
    title: this.building.location,
    content: this.building.location_description,
    icon: 'fa fa-map-marker',
  };



  public data_amenities = {
    title: 'Amenities',
    icon: 'fa fa-diamond',
    list: this.building.amenities,
  };




  public data_how_it_works = {
    title: 'How it works',
    content: this.building.how_it_works,
    icon: 'fa fa-bullseye',
  };



  public data_nearby = {
    title: 'Nearby',
    icon: 'fa fa-thumb-tack',
    list: this.building.nearbys,
  };
  public data_building_facts = {
    title: 'Building Facts',
    icon: 'fa  fa-plug',
    list: this.building.facts,
  };

  public data_carrusel = {
    breadcrumb: this.building.categorys,
    images: this.building.images,
  };

  public data_description_build = {
    title: this.building.name,
    content: this.building.description,
    icon: 'fa fa-building-o',
  };
 


}
