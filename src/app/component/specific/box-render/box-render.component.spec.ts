import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxRenderComponent } from './box-render.component';

describe('BoxRenderComponent', () => {
  let component: BoxRenderComponent;
  let fixture: ComponentFixture<BoxRenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxRenderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
