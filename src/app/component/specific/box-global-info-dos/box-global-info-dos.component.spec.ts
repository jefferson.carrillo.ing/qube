import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalInfoDosComponent } from './box-global-info-dos.component';

describe('BoxGlobalInfoDosComponent', () => {
  let component: BoxGlobalInfoDosComponent;
  let fixture: ComponentFixture<BoxGlobalInfoDosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalInfoDosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalInfoDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
