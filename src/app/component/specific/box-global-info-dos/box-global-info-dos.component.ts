import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-info-dos',
  templateUrl: './box-global-info-dos.component.html',
  styleUrls: ['./box-global-info-dos.component.css'],
})
export class BoxGlobalInfoDosComponent implements OnInit {
  @Input() public availabilitys: any;
  @Input() public building: any;
  @Input() public suite: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
