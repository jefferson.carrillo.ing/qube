import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalInfoTresComponent } from './box-global-info-tres.component';

describe('BoxGlobalInfoTresComponent', () => {
  let component: BoxGlobalInfoTresComponent;
  let fixture: ComponentFixture<BoxGlobalInfoTresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalInfoTresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalInfoTresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
