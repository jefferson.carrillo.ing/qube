import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxCarruselComponent } from './box-carrusel.component';

describe('BoxCarruselComponent', () => {
  let component: BoxCarruselComponent;
  let fixture: ComponentFixture<BoxCarruselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxCarruselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxCarruselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
