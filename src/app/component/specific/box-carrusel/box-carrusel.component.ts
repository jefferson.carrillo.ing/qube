import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-carrusel',
  templateUrl: './box-carrusel.component.html',
  styleUrls: ['./box-carrusel.component.css'],
})
export class BoxCarruselComponent implements OnInit {
  @Input() public data: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
