import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalContentBlockComponent } from './box-global-content-block.component';

describe('BoxGlobalContentBlockComponent', () => {
  let component: BoxGlobalContentBlockComponent;
  let fixture: ComponentFixture<BoxGlobalContentBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalContentBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalContentBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
