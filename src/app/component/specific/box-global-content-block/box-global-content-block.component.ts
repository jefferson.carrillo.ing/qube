import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-content-block',
  templateUrl: './box-global-content-block.component.html',
  styleUrls: ['./box-global-content-block.component.css'],
})
export class BoxGlobalContentBlockComponent implements OnInit {
  @Input() public data: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
