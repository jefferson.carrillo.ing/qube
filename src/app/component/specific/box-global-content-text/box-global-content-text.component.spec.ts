import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalContentTextComponent } from './box-global-content-text.component';

describe('BoxGlobalContentTextComponent', () => {
  let component: BoxGlobalContentTextComponent;
  let fixture: ComponentFixture<BoxGlobalContentTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalContentTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalContentTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
