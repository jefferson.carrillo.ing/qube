import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-content-text',
  templateUrl: './box-global-content-text.component.html',
  styleUrls: ['./box-global-content-text.component.css']
})
export class BoxGlobalContentTextComponent implements OnInit {
  @Input() public data: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
