import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-geolocation',
  templateUrl: './box-global-geolocation.component.html',
  styleUrls: ['./box-global-geolocation.component.css'],
})
export class BoxGlobalGeolocationComponent implements OnInit {
  @Input() public data: any; 
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {}
}
