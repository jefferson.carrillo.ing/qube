import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalGeolocationComponent } from './box-global-geolocation.component';

describe('BoxGlobalGeolocationComponent', () => {
  let component: BoxGlobalGeolocationComponent;
  let fixture: ComponentFixture<BoxGlobalGeolocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalGeolocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalGeolocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
