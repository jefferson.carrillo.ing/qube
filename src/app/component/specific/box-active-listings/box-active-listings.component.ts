import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

declare var $: any;
@Component({
  selector: 'app-box-active-listings',
  templateUrl: './box-active-listings.component.html',
  styleUrls: ['./box-active-listings.component.css'],
})
export class BoxActiveListingsComponent implements OnInit {
  @Input() public availabilitys: any;
  @Input() public building: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() {}

  ngOnInit(): void {
    var availabilitys = this.availabilitys;
    function render_availability(e: any, type: any, row: any, meta: any) {
      return `<span class="label" style="color: black; background: ${availabilitys[e].color};">${availabilitys[e].name}</span>`;
    }
    function render_unit(e: any, type: any, row: any, meta: any) {
      return `<a href="/suite?id=${row.id}" target="_blank" class="deco" >${row.type} (${row.floor}${row.room}) ${row.unit}</a>`;
    }

    $('#id_tabala_active_listings').DataTable({
      data: this.building.suites,
      scrollY: 300,
      scrollX: true,
      columnDefs: [
        {
          targets: '_all',
          createdCell: function (
            td: any,
            cellData: any,
            rowData: any,
            row: any,
            col: any
          ) {
            $(td).css('padding-left', '15px');
          },
        },
      ],
      columns: [
        { title: 'Unit', data: 'unit', render: render_unit },
        { title: 'Sales Price', data: 'price_sale' },
        { title: 'Rental Price', data: 'price_rent' },
        {
          title: 'Availability',
          data: 'id_availability',
          render: render_availability,
        },
        { title: 'Size(sq.m.)', data: 'size_m2' },
        { title: 'Size(sq.ft.)', data: 'size_ft2' },
      ],
    });
  }
}
