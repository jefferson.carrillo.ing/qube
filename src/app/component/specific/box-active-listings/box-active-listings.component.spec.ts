import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxActiveListingsComponent } from './box-active-listings.component';

describe('BoxActiveListingsComponent', () => {
  let component: BoxActiveListingsComponent;
  let fixture: ComponentFixture<BoxActiveListingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxActiveListingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxActiveListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
