import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxGlobalInfoTeamComponent } from './box-global-info-team.component';

describe('BoxGlobalInfoTeamComponent', () => {
  let component: BoxGlobalInfoTeamComponent;
  let fixture: ComponentFixture<BoxGlobalInfoTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxGlobalInfoTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxGlobalInfoTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
