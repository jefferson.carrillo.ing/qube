import { Component, Input, OnInit } from '@angular/core';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-box-global-info-team',
  templateUrl: './box-global-info-team.component.html',
  styleUrls: ['./box-global-info-team.component.css']
})
export class BoxGlobalInfoTeamComponent implements OnInit {
  @Input() public data: any;
  public id_box = Utilitarian.GenerateKeyHex();
  constructor() { }

  ngOnInit(): void {
  }

}
