import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeneralModel } from '../../models/GeneralModel';
import { Utilitarian } from './../../services/Utilitarian';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent implements OnInit {
  public dataGeneral: GeneralModel = new GeneralModel();


  public dataBoxRender: any = [
    { section: "left", box_class: "in", type: "html", key: "#company_information" },
    { section: "left", box_class: "in", type: "html", key: "#we_are_future" },
    { section: "left", box_class: "in", type: "team", key: "#info-team" },
    { section: "left", box_class: "in", type: "contact", key: "#contact-us" },
    { section: "right", box_class: "in", type: "line", key: "#line" },
    { section: "right", box_class: "in", type: "html", key: "#what_we_do" },
    { section: "right", box_class: "in", type: "awards", key: "#awards" },
    { section: "right", box_class: "in", type: "geolocation", key: "#geolocation" },
  ]

  constructor(private activatedRoute: ActivatedRoute) { }
  ngOnInit(): void {
    Utilitarian.JumpTo(this.activatedRoute); 

  }
 


}
