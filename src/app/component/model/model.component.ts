import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeneralModel } from 'src/app/models/GeneralModel';
import { Utilitarian } from 'src/app/services/Utilitarian';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {

  public dataGeneral: GeneralModel = new GeneralModel();
  public dataBoxRender: any = [
    { section: "left", box_class: "in", type: "html", key: "#global_box_1" },
    { section: "left", box_class: "in", type: "html", key: "#global_box_2" },
    { section: "left", box_class: "in", type: "html", key: "#global_box_3" },
    { section: "right", box_class: "in", type: "html", key: "#global_box_4" },
    { section: "right", box_class: "in", type: "html", key: "#global_box_5" },
    { section: "right", box_class: "in", type: "html", key: "#global_box_6" },
    { section: "right", box_class: "in", type: "html", key: "#global_box_7" },
  ];


  constructor(private activatedRoute: ActivatedRoute) { }
  ngOnInit(): void {
    Utilitarian.JumpTo(this.activatedRoute);
  }

}
