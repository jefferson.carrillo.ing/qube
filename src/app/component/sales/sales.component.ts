import { Component, OnInit } from '@angular/core';
import { Availabilitys, Building } from 'data_base';
import { GeneralModel } from 'src/app/models/GeneralModel';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
  public dataGeneral: GeneralModel = new GeneralModel();



  public dataBoxRender: any = [
    { section: "left", box_class: "in", type: "html", key: "#invest_with_us" },
    { section: "left", box_class: "in", type: "html", key: "#how_it_works" },
    { section: "left", box_class: "in", type: "html", key: "#sale-office" },
    { section: "left", box_class: "in", type: "contact", key: "#contact-us" },
    { section: "left", box_class: "in", type: "geolocation", key: "#geolocation" },

    { section: "right", box_class: "in", type: "info", key: "#active_listings" },
    { section: "right", box_class: "in", type: "carrusel", key: "#carrusel" },
    { section: "right", box_class: "in", type: "html", key: "#topical-loving" },
    { section: "right", box_class: "in", type: "amenities", key: "#amenities" },
    { section: "right", box_class: "in", type: "nearby", key: "#nearby" },
    { section: "right", box_class: "in", type: "building_facts", key: "#building_facts" },

    { section: "bottom", box_class: "in", type: "active-listings", key: "#active-listings" },
    { section: "bottom", box_class: "in", type: "asked-questions", key: "#asked-questions" },
  ]


  ngOnInit(): void { }






}
